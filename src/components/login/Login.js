import React from 'react';
import {loginUser} from '../../services/ddiAPI'
import './Login.css'

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authSuccess: true,
            id: '',
            email: '',
            password: '',
            name: '',
            surname: '',
            dob: '',
            address: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    async login(event) {
        event.preventDefault();
        const res = await loginUser(this.state.email, this.state.password)
        if (res.error) {
            this.setState({authSuccess: false})
        }
        if (res.success) {
            this.setState({
                id: res.id,
                name: res.name,
                surname: res.surname,
                dob: res.dob,
                address: res.address
            })
            this.props.history.push({
                pathname : '/users/' + this.state.id,
                state: {
                name: this.state.name,
                surname: this.state.surname,
                dob: this.state.dob,
                address: this.state.address
            }
        } 
        );
    }
}

    render() {
        const isLoggedIn = this.state.authSuccess;
        let message;
        if (!isLoggedIn) {
            message = <p className="log-in-message">Incorrect email or password, try again!</p>
        }
        return (
            <div className="row">
                <div className="small-12 medium-6 columns" id="login">
                <h4 className="text-center">Welcome to DDI</h4>
                <form className="log-in-form" onSubmit={this.login}>
                    <h4 className="text-center">Log in with you email account</h4>
                    <label>
                        Email:
                    <input type="email" name="email" value={this.state.value} onChange={this.handleChange} />
                    </label>
                    <label>
                        Password:
                        <input type="password" name="password" value={this.state.value} onChange={this.handleChange} />
                    </label>
                    <input type="submit" className="button expanded" value="Login" />
                    {message}
                </form>
                </div>
            </div>
        );
    }
}

export default LoginForm;
